from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

from blogapp import views as blogviews
import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'djentlesite.views.home', name='home'),
    # url(r'^djentlesite/', include('djentlesite.foo.urls')),
    url(r'^$', blogviews.PostsView.as_view(template_name='home.html')),
    url(r'^blogposts/(?P<pk>\d+)/$', blogviews.PostView.as_view(template_name='blogpost.html')),
    url(r'^blogposts/(?P<pk>\d+)/edit/$', blogviews.PostEditView.as_view(
        template_name='blogpostedit.html', base_success_url='/blogposts')),
    url(r'^blogposts/add/$', blogviews.PostAddView.as_view(
        template_name='blogpostadd.html', base_success_url='/blogposts')),
    url(r'^blogposts/(?P<pk>\d+)/delete/$', blogviews.PostDeleteView.as_view(
        template_name='blogapp/post_confirm_delete.html', success_url='/')),
    url(r'^hello/$', TemplateView.as_view(template_name='helloworld.html')),
    url(r'^contact/$', views.ContactView.as_view()),
    url(r'^contactconfirm/$', TemplateView.as_view(template_name='contactconfirm.html')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^blogapp/', include('blogapp.urls')),
)
