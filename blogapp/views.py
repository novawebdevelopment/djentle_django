from django.views import generic
from django import forms
from django.utils import timezone
from django.http import HttpResponseRedirect

from blogapp import models

class PostsView(generic.ListView):
    template_name = 'posts.html'
    context_object_name = 'posts'

    def get_queryset(self):
        """Return all posts."""
        return models.Post.objects.all()

class PostView(generic.DetailView):
    model = models.Post
    template_name = 'post.html'

class PostForm(forms.ModelForm):
    class Meta:
        model = models.Post
        exclude = ['date_posted']

class PostEditView(generic.UpdateView):
    form_class = PostForm
    model = models.Post
    template_name = 'editpost.html'
    base_success_url = '/blogapp/posts'

    def get_success_url(self):
        return '%s/%s' % (self.base_success_url, self.object.id)

class PostAddView(generic.CreateView):
    form_class = PostForm
    model = models.Post
    template_name = 'addpost.html'
    base_success_url = '/blogapp/posts'

    def get_success_url(self):
        return '%s/%s' % (self.base_success_url, self.object.id)

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.date_posted = timezone.now()
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())

class PostDeleteView(generic.DeleteView):
    model = models.Post
    success_url = '/blogapp/posts'

